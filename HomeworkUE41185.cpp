﻿// HomeworkUE41185.cpp : 
// Понять принципы работы с памятью и научиться работать с ней при помощи указателей.

#include "MyLittleHelpers.h" // мои вспомогательные функции
#include <iostream>
#include <utility> // для swap
#include <iomanip> // необходимо для выравнивания таблицы

#define OffSetUp 5 // строка отступа от верхнего края

class ScoreboardRow  // класс "строка таблицы счета"
{
private:

    char pname[14]; // имя 14 символов
    int score = 0;  // количество очков

public:

    void FeedData()  // запрос данных, вводим с консоли
    {
        TConsole Console;  // нужно для игры цвета
        Console.GotoXY(0, OffSetUp); // задаем отступ вывода
        Console.TextColor(COLOR_GREEN); // задаем цвет текста вывода
        std::cout << "+----------------------------+\n";
        std::cout << "| Введите имя игрока: |+-----+\n";
        Console.TextColor(COLOR_LIGHTRED);
        std::cout << "[]- > ";
        std::cin >> pname; // считываем имя игрока
        Console.GotoXY(0, OffSetUp);
        std::cout << "                              \n"; // затираем предыдущий вывод в консоль
        std::cout << "                              \n";
        std::cout << "                              \n";
        Console.GotoXY(0, OffSetUp);                     // переставляем курсор в нужную позицию вывода
        std::cout << "+----------------------------+\n";
        Console.TextColor(COLOR_LIGHTBLUE);
        std::cout << "| Сколько очков он набрал? |++\n";
        Console.TextColor(COLOR_LIGHTRED);
        std::cout << "[]- > ";
        std::cin >> score;
        Console.GotoXY(0, OffSetUp);
        std::cout << "                              \n"; // затираем предыдущий вывод в консоль
        std::cout << "                              \n";
        std::cout << "                              \n";
        Console.GotoXY(0, OffSetUp);
    }
    void IssueData() // просто красивый вывод данных
    {
        std::cout << "|" << std::setw(18) << pname << " | " << std::setw(6) << score << " |" << std::endl;
    }
    int GetScore() // вывод количества очков
    {
        return score;
    }
};

void gnomeSort(ScoreboardRow* arr, int n) // сортировка гномиками, не оптимизированная, так как не ожидается большой массив  
{
    if (arr)
    {
    int index = 0;

    while (index < n) {  // пока есть что переставлять, переставляем
        if (index == 0)
            index++;
        if (arr[index].GetScore() <= arr[index - 1].GetScore()) // большие значения перемещаем вверх таблицы
            index++;
        else {
            std::swap(arr[index], arr[index - 1]); // непосредственно перестановка объектов в массиве
            index--;
        }
    }
    }
};

int main()
{
    ConsoleCP cp(1251); //переключаем ввод и вывод консоли на кириллицу
    TConsole Console; // нужно для игры цвета

    int arrSize = 0;

    Console.TextColor(COLOR_GREEN); // задаем цвет текста вывода 
    std::cout << "+----------------------------+\n";
    std::cout << "| Введите количество игроков:|\n";
    Console.TextColor(COLOR_LIGHTRED);
    std::cout << "[]- > ";
    std::cin >> arrSize;

    ScoreboardRow* ScoreArrey = new ScoreboardRow [arrSize]; // объявляем динамический массив объектов нашего класса

    for (int i = 0; i < arrSize; ++i) // инициализация массива значениями, введёнными с консоли
    {
        Console.GotoXY(0, 3); // задаем отступ вывода
        Console.TextColor(COLOR_YELLOW);
        std::cout << "+----------------------------+\n";
        std::cout << "| Осталось ввести: " << std::setw(9) << arrSize-i << " |\n";
        ScoreArrey[i].FeedData();
    }
    Console.GotoXY(0, 3);
    Console.TextColor(COLOR_YELLOW);
    std::cout << "+----------------------------+\n";
    std::cout << "| Осталось ввести: " << std::setw(9) << 0 << " |\n";

    Console.GotoXY(0, 5);
    Console.TextColor(COLOR_LIGHTRED);
    std::cout << "+----------------------------+\n";
    std::cout << "+ Неотсортированная таблица: +\n";
    std::cout << "+----------------------------+\n";
    for (int i = 0; i < arrSize; ++i) // демонстрация введённых значений
    {
        ScoreArrey[i].IssueData();
    }
    std::cout << "+----------------------------+\n";

    gnomeSort(ScoreArrey, arrSize); // сортировочных гномиков вызывали?

    Console.TextColor(COLOR_CYAN);
    std::cout << "+----------------------------+\n";
    std::cout << "+ Отсортированная таблица:   +\n";
    std::cout << "+----------------------------+\n";
    for (int i = 0; i < arrSize; ++i) // демонстрация результата
    {
       ScoreArrey[i].IssueData();
    }
    std::cout << "+----------------------------+\n";

    Console.TextColor(COLOR_LIGHTGRAY); // завершаем цветную революцию

    delete[] ScoreArrey; // выгоняем гномиков, освобождаем память

    return 0;
}
